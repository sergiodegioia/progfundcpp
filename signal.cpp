#include "signal.hpp"
#include <iostream>
#include <sstream>
using namespace std;


Signal::Signal( double from, double to, unsigned int len): from( from), to( to), len( len){
    val = new double[ len];
    for( int i = 0; i < len; i++){
        val[ i] = .0;
    }
    stepSz = (to - from)/len;
    cout << "Signal constr called: from = " << from << endl;
}

Signal::Signal( const Signal&& rhs):
        from( std::move( rhs.from)),
        to( std::move( rhs.to)),
        stepSz( std::move( rhs.stepSz)),
        len( std::move( rhs.len)),
        val( std::move( new double[ len]))
    {
        cout << "Signal move cnstr" << endl;
}

Signal::Signal( const Signal& rhs):
        from( rhs.from),
        to( rhs.to),
        stepSz( rhs.stepSz),
        len( rhs.len),
        val( new double[ len])
    {
        for( int i = 0; i < len; i++){
            val[ i] = rhs.val[ i];
        }
        cout << "Signal copy cnstr" << endl;
}

Signal &Signal::operator=( const Signal& rhs){
    from = rhs.from;
    to = rhs.to;
    stepSz = rhs.stepSz;
    if( len != rhs.len){
        len = rhs.len;
        delete [] val;
        val = new double[ len];
        for( int i = 0; i < len; i++){
            val[ i] = rhs.val[ i];
        }
    }
    cout << "Signal copy assignment" << endl;
    return *this;
}

double &Signal::operator[]( const int pos){ return val[ pos];}

double &Signal::operator[]( const int pos) const { return val[ pos];}

Signal::operator double() const{ return val[ len - 1];}

ostream& operator<<( ostream& os, const Signal sgl){
    auto len = sgl.get_len();
    auto from = sgl.get_from();
    auto stepSz = sgl.get_stepSz();
    for( int i = 0; i < len; i++){
        os << sgl[ i] << ' ';
    }
    os << "\n";
    for( int i = 0; i < len; i++){
        os << (from + i * stepSz) << ' ';
    }
    return os << "\n";
}

Signal::~Signal(){
    delete[] val;
}

Signal Signal::operator*=( const Signal& rhs){
    unsigned conv_len = len + rhs.len - 1;
    Signal conv( from, conv_len * stepSz, conv_len);
    cout << "conv len = " << conv.get_len() << endl;
    unsigned min_len = rhs.len;
    if( len < min_len){
        min_len = len;
    }
    for( int i = 0; i < conv.get_len(); i++){
        double a = .0;
        for( int j = 0; j < min_len && j < i + 1; j++){
            a += val[ i - j]* rhs.val[ j];
            //cout << "val[ " << i << " - " << j << "] = " << val[ i - j] << "; rhs.val[ " << j << "] = " << rhs.val[ j] << endl;
        }
        conv[ i] = a;
    }
    *this = conv;
    return *this;
}

Signal operator*( const Signal& lhs, const Signal& rhs){
    Signal conv( lhs);
    return conv *= rhs;
}

double Signal::get_from() const { return from;}
double Signal::get_to() const { return to;}
double Signal::get_stepSz() const { return stepSz;}
double Signal::get_coord( const int pos) const { return from + pos * stepSz;}
unsigned int Signal::get_len() const { return len;}

std::string Signal::to_csv() const {
    std::stringstream ss;
    for( int i = 0; i < len; i++){
        ss << val[ i] << ' ';
    }
    ss << " \n";
    for( int i = 0; i < len; i++){
        ss << (from + i * stepSz) << ' ';
    }
    ss << " \n ";
    return ss.str();
}

