#include <iostream>

using namespace std;

class Clazz{
	public:
		Clazz();
		Clazz( Clazz& rhs);
		Clazz( std::string name);
		Clazz( const Clazz& rhs);
		Clazz( Clazz&& rhs);
		Clazz( const Clazz&& rhs);
		~Clazz();
		std::string get_name();
	private:
		std::string name;
};

Clazz::Clazz(){
	cout << "noarg cstr" << endl;
}

Clazz::Clazz( std::string name): name( name){
	cout << "ordinary cstr name = " << name << endl;
}

Clazz::Clazz( Clazz& rhs){
	cout << "copy cstr" << endl;
}

Clazz::Clazz( const Clazz& rhs){
	cout << "copy const cstr" << endl;
}

Clazz::Clazz( Clazz&& rhs){
	cout << "move cstr" << endl;
}

Clazz::Clazz( const Clazz&& rhs){
	cout << "move const cstr" << endl;
}

Clazz::~Clazz(){
	cout << "dcstr: " << name << endl;
}

// Clazz& fun(){
// 	Clazz clazz("fun");
// 	return clazz;
// }

std::string Clazz::get_name(){
	    return name;
}

// int main(){
// 	cout << "Hello, c++!!" << endl;
// 	Clazz clazz;
// 	Clazz clazz1( clazz);
// 	Clazz clazz2( std::move( clazz));
// 	Clazz clazz3 = fun();
// 	const Clazz &clazz4 = *new Clazz("clazz4");
// }


