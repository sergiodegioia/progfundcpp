#include <string>
#include <iostream>
using namespace std;

class Signal{
    public:
        Signal( double from, double to, unsigned int len);
        Signal( const Signal&);
        Signal( const Signal&&);
        Signal &operator=( const Signal& sd);
        operator double() const;
        double &operator[]( const int pos);
        double &operator[]( const int pos) const;
        virtual ~Signal();
        Signal operator*=( const Signal& rhs);
        double get_from() const;
        double get_to() const;
        double get_stepSz() const;
        double get_coord( const int pos) const;
        unsigned int get_len() const;
        std::string to_csv() const;
    private:
        unsigned int len;
        double from, to, stepSz;
        double *val;
};
Signal operator*(const Signal& lhs, const Signal& rhs);

Signal createSignal( const int);

ostream& operator<<( ostream& os, const Signal sgl);
