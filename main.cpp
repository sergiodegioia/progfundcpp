#include <iostream>
#include <fstream>
#include <math.h>
#include "signal.hpp"

using namespace std;
const unsigned LEN = 1024;
const double PHI_0 = .0;

void plot( const Signal &sgl, const std::string &outfilename);
Signal createSinc();
Signal createSinc(){
    Signal sgl( PHI_0, PHI_0 + 20 * M_PI_2, LEN);
    for( int i = 0; i < sgl.get_len(); i++){
        if( 0 == sgl.get_coord( i)){
            sgl[ i] = 1;
        }else{
            sgl[ i] = sin( sgl.get_coord( i))/sgl.get_coord( i);
        }
    }
    cout << "just before returning from createSinc()" << endl;
    return sgl;
}
Signal createSincSin();
Signal createSincSin(){
    Signal sgl( PHI_0, PHI_0 + 20000 * M_PI_2, LEN);
    for( int i = 0; i < sgl.get_len(); i++){
        if( 0 == sgl.get_coord( i)){
            sgl[ i] = 1;
        }else{
            sgl[ i] = sin( 1000 * sgl.get_coord( i)) * sin( sgl.get_coord( i))/sgl.get_coord( i);
        }
    }
    return sgl;
}
int main(){
    auto sgl = createSinc();
    cout << "just after calling createSinc()" << endl;
    auto secondSgl = createSincSin();
    auto conv = sgl * secondSgl;
    plot( sgl, "out.csv");
    //cout << sgl << endl;
    return 0;
}

void plot( const Signal &sgl, const std::string &outfilename){
    std::ofstream output{ outfilename};
    // output << sgl.to_csv();
    output << sgl << sgl;
}